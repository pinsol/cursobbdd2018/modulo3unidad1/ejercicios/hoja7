﻿/* hoja 7 - unidad 1 - modulo 3 - EJERCICIO 3*/

  DROP DATABASE IF EXISTS h7u1m3EJ3;
CREATE DATABASE IF NOT EXISTS h7u1m3EJ3;
USE h7u1m3EJ3;
 
-- crear tabla TURISTA
  DROP TABLE IF EXISTS turista;
  CREATE TABLE IF NOT EXISTS turista(
    t int,
    nombre varchar(15),
    apellidos varchar(15),
    dir varchar(25),
    tel varchar(15),
    PRIMARY KEY (t)
    );

-- crear tabla VUELO
 DROP TABLE IF EXISTS vuelo;
 CREATE TABLE IF NOT EXISTS vuelo(
    n int,
    numTurista varchar(15),
    fecha date,
    hora char(8),
    origen varchar(15),
    destino varchar(15),
    numTotal char(6),
    PRIMARY KEY (n)
);

-- crear tabla AGENCIA
  DROP TABLE IF EXISTS agencia;
  CREATE TABLE IF NOT EXISTS agencia(
    s int,
    direccion varchar(15),
    tel varchar(15),
    PRIMARY KEY (s)
    );

-- crear tabla HOTEL
  DROP TABLE IF EXISTS hotel;
  CREATE TABLE IF NOT EXISTS hotel(
    h int,
    nombre varchar(15),
    dir varchar(25),
    ciudad varchar(15),
    tel varchar(15),
    plazas int,
    PRIMARY KEY (h)
    );


  -- crear tabla toma
  CREATE TABLE IF NOT EXISTS toma(
    turista int,
    vuelo int,
    clase char(8),
    PRIMARY KEY (turista, vuelo),
    CONSTRAINT FKtomaTurista FOREIGN KEY(turista)
      REFERENCES turista(t) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT FKtomaVuelo FOREIGN KEY(vuelo)
      REFERENCES vuelo(n) ON DELETE CASCADE ON UPDATE CASCADE   
    );

-- crear tabla contrata
  CREATE TABLE IF NOT EXISTS contrata(
    turista int,
    agencia int,
    PRIMARY KEY (turista, agencia),
    CONSTRAINT FKcontrataTurista FOREIGN KEY(turista)
      REFERENCES turista(t) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT FKcontrataAgencia FOREIGN KEY(agencia)
      REFERENCES agencia(s) ON DELETE CASCADE ON UPDATE CASCADE   
    );

  -- crear tabla reserva
  CREATE TABLE IF NOT EXISTS reserva(
    turista int,
    hotel int,
    fechaE date,
    fechaS date,
    pension char(7),
    PRIMARY KEY (turista, hotel),
    CONSTRAINT FKreservaTurista FOREIGN KEY(turista)
      REFERENCES turista(t) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT FKreservaHotel FOREIGN KEY(hotel)
      REFERENCES hotel(h) ON DELETE CASCADE ON UPDATE CASCADE   
    );