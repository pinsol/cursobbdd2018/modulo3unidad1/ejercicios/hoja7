﻿/*
  hoja 7 - unidad 1 - modulo 3 - ejercicio 4
*/

-- base de datos

  DROP DATABASE IF EXISTS hoja7unidad1modulo3ejercicio4;

  CREATE DATABASE IF NOT EXISTS hoja7unidad1modulo3ejercicio4;

  USE hoja7unidad1modulo3ejercicio4;


-- grupo

  DROP TABLE IF EXISTS grupo;

  CREATE TABLE IF NOT EXISTS grupo(
    n int,
    nombre varchar(50),
    PRIMARY KEY(n)
    );


  -- creacion de la tabla alumno

  DROP TABLE IF EXISTS alumno;
    CREATE TABLE IF NOT EXISTS alumno(
      n int,
      dni char(9),
      nombre varchar(30),
      PRIMARY KEY(n)
      );


    /* 
    DROP TABLE IF EXISTS alumno;

    CREATE TABLE IF NOT EXISTS  alumno_estaen_dirige(
    n varchar(15),
    dni varchar(15),
    nombre varchar(15),
    ngrupo varchar(15),
    dni_profe varchar(15),
    PRIMARY KEY (n),
    CONSTRAINT fkestaengrupo FOREIGN KEY(ngrupo)
      REFERENCES grupo(n)  ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT fkdirigeprofe FOREIGN KEY (dni)
    REFERENCES profesor(dni_profe)  ON DELETE CASCADE ON UPDATE CASCADE
    );
    */

  --  Aportación de Imanol, tabla Profesores
  CREATE TABLE IF NOT EXISTS profesor(
    dni char(9),
    nombre varchar(30),
    domicilio varchar(30),
    PRIMARY KEY (dni)
  );

 -- INSERT INTO  profesor VALUES ('33344422l','Ramón','Casa Ramón');
      

  CREATE TABLE IF NOT EXISTS colabora(
  alumno int,
  profesor char(9),
  PRIMARY KEY (alumno,profesor),
  CONSTRAINT FKColaboraAlumno FOREIGN KEY (alumno)
  REFERENCES alumno(n) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT fkColaboraProfesor FOREIGN KEY (profesor)
  REFERENCES profesor(dni) ON DELETE CASCADE ON UPDATE CASCADE
  );


  -- tabla proyecto fin de carrera
  CREATE TABLE tfc(
    n int,
    nAlumno int,
    tema varchar(15),
    fecha date,
    PRIMARY KEY (n, nAlumno),
    CONSTRAINT FKTFCAlumno FOREIGN KEY (NAlumno)
    REFERENCES alumno(n) ON DELETE CASCADE ON UPDATE CASCADE
    );


   -- tabla TRIBUNAL
  DROP TABLE IF EXISTS tribunal;

  CREATE TABLE IF NOT EXISTS tribunal(
    n int,
    lugar varchar(50),
    PRIMARY KEY (n)
    );

--
-- Create table `realizan`
--

CREATE TABLE examinan (
  nTribunal int(11) NOT NULL,
  nTFC int(11) NOT NULL,
  nAlumno int,
  fecha date,
  PRIMARY KEY (nTribunal,nTFC,nAlumno),
  CONSTRAINT examinanUnique1 UNIQUE KEY(nTribunal),
  CONSTRAINT examinanUnique2 UNIQUE KEY (nTFC,nAlumno),
  CONSTRAINT FK_examinan_nTribunal FOREIGN KEY (nTribunal)
    REFERENCES tribunal(n) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT FK_examinan_nTFC FOREIGN KEY (nTFC,nAlumno)
    REFERENCES tfc (n,nAlumno) ON DELETE CASCADE ON UPDATE CASCADE
);


-- tabla dirige
  DROP TABLE IF EXISTS dirige;

  CREATE TABLE IF NOT EXISTS dirige(
    alumno int,
    profesor char(9),
    PRIMARY KEY(alumno,profesor),
    CONSTRAINT dirigeunica1 UNIQUE KEY(alumno),
    CONSTRAINT FKdirigeAlumno FOREIGN KEY (alumno)
      REFERENCES alumno(n) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT  FKdirigeProfesor FOREIGN KEY (profesor)
      REFERENCES profesor(dni) ON DELETE CASCADE ON UPDATE CASCADE 
    );


  -- tabla esta en

    DROP TABLE IF EXISTS estaEn;

    CREATE TABLE IF NOT EXISTS estaEn(
      grupo int,
      alumno int,
      PRIMARY KEY (grupo,alumno),
      CONSTRAINT FKestaEnGrupo FOREIGN KEY (grupo)
        REFERENCES grupo(n) ON DELETE CASCADE ON UPDATE CASCADE,
      CONSTRAINT FKestaEnAlumno FOREIGN KEY (alumno)
        REFERENCES  alumno(n) ON DELETE CASCADE ON UPDATE CASCADE
      );

  
  -- tabla pertenece

  DROP TABLE IF EXISTS pertenece;

  CREATE TABLE IF NOT EXISTS pertenece(
    profesor char(9),
    tribunal int,
    PRIMARY KEY(profesor,tribunal),
    CONSTRAINT FKperteneceProfesor FOREIGN KEY (profesor)
      REFERENCES profesor(dni) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT FKperteneceTribunal FOREIGN KEY (tribunal)
      REFERENCES tribunal(n) ON DELETE CASCADE ON UPDATE CASCADE
    );
