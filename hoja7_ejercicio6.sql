﻿﻿/* hoja 7 - unidad 1 - modulo 3 - EJERCICIO 6*/

  DROP DATABASE IF EXISTS h7u1m3Ej6;
CREATE DATABASE IF NOT EXISTS h7u1m3Ej6;
USE h7u1m3Ej6;
 
-- crear tabla VEHICULO
  DROP TABLE IF EXISTS vehiculo;
  CREATE TABLE IF NOT EXISTS vehiculo(
    mat char(7),
    marca varchar(15),
    modelo varchar(15),
    precio varchar(25),
    PRIMARY KEY (mat)
    );

-- crear tabla VENDEDOR
 DROP TABLE IF EXISTS vendedor;
 CREATE TABLE IF NOT EXISTS vendedor(
    dni char(9),
    nombre varchar(15),
    dir varchar(15),
    tel varchar(15),
    PRIMARY KEY (dni)
);

-- crear tabla CLIENTE
  DROP TABLE IF EXISTS cliente;
  CREATE TABLE IF NOT EXISTS cliente(
    dni char(9),
    nombre varchar(15),
    dir varchar(15),
    tel varchar(15),
    PRIMARY KEY (dni)
    );

  -- crear tabla OPCION
  DROP TABLE IF EXISTS opcion;
  CREATE TABLE IF NOT EXISTS opcion(
    nombre varchar(15),
    tel varchar(25),
    PRIMARY KEY (nombre)
    );

-- crear tabla MODELO
  DROP TABLE IF EXISTS modelo;
  CREATE TABLE IF NOT EXISTS modelo(
    marca varchar(15),
    cil varchar(15),
    modelo varchar(15),
    PRIMARY KEY (marca, cil, modelo)
    );


  -- crear tabla tiene
  CREATE TABLE IF NOT EXISTS toma(
    turista int,
    vuelo int,
    clase char(8),
    PRIMARY KEY (turista, vuelo),
    CONSTRAINT FKtomaTurista FOREIGN KEY(turista)
      REFERENCES turista(t) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT FKtomaVuelo FOREIGN KEY(vuelo)
      REFERENCES vuelo(n) ON DELETE CASCADE ON UPDATE CASCADE   
    );

-- crear tabla contrata
  CREATE TABLE IF NOT EXISTS contrata(
    turista int,
    agencia int,
    PRIMARY KEY (turista, agencia),
    CONSTRAINT FKcontrataTurista FOREIGN KEY(turista)
      REFERENCES turista(t) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT FKcontrataAgencia FOREIGN KEY(agencia)
      REFERENCES agencia(s) ON DELETE CASCADE ON UPDATE CASCADE   
    );

  -- crear tabla cede
  CREATE TABLE IF NOT EXISTS reserva(
    turista int,
    hotel int,
    fechaE date,
    fechaS date,
    pension char(7),
    PRIMARY KEY (turista, hotel),
    CONSTRAINT FKreservaTurista FOREIGN KEY(turista)
      REFERENCES turista(t) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT FKreservaHotel FOREIGN KEY(hotel)
      REFERENCES hotel(h) ON DELETE CASCADE ON UPDATE CASCADE   
    );
