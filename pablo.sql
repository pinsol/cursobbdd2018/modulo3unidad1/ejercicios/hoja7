﻿/*
  hoja 7 - unidad 1 - modulo 3 - ejercicio 4
*/

-- base de datos

  DROP DATABASE IF EXISTS hoja7unidad1modulo3ejercicio4;

  CREATE DATABASE IF NOT EXISTS hoja7unidad1modulo3ejercicio4;

  USE hoja7unidad1modulo3ejercicio4;

 -- tabla TRIBUNAL
  DROP TABLE IF EXISTS tribunal;
  CREATE TABLE IF NOT EXISTS tribunal(
    n int,
    lugar varchar(50),
    PRIMARY KEY (n)
    );