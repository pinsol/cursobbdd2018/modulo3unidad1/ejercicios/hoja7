﻿/* hoja 7 - unidad 1 - modulo 3 - EJERCICIO 2*/

  DROP DATABASE IF EXISTS h7u1m3EJ2;
CREATE DATABASE IF NOT EXISTS h7u1m3EJ2;
USE h7u1m3EJ2;
 
-- crear tabla COMPANIA
  DROP TABLE IF EXISTS compania;
  CREATE TABLE IF NOT EXISTS compania(
    numCompania int,
    actividad varchar(15),
    PRIMARY KEY (numCompania)
    );

-- crear tabla SOLDADO
 DROP TABLE IF EXISTS soldado;
 CREATE TABLE IF NOT EXISTS soldado(
    codSoldado int,
    nombre varchar(15),
    apellidos varchar(15),
    grado varchar(10),
    PRIMARY KEY (codSoldado)
);

-- crear tabla perteneceCompania
  CREATE TABLE IF NOT EXISTS perteneceCompania(
    compania int,
    soldado int,
    PRIMARY KEY (compania, soldado),
    CONSTRAINT FKperteneceCompaniaCompania FOREIGN KEY(compania)
      REFERENCES compania(numCompania) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT FKperteneceCompaniaSoldado FOREIGN KEY(soldado)
      REFERENCES soldado(codSoldado) ON DELETE CASCADE ON UPDATE CASCADE   
    );


-- crear tabla SERVICIO
  DROP TABLE IF EXISTS servicio;
  CREATE TABLE IF NOT EXISTS servicio(
    codServicio int,
    descripcion varchar(15),
    PRIMARY KEY (codServicio)
    );

-- crear tabla realiza
  CREATE TABLE IF NOT EXISTS realiza(
    soldado int,
    servicio int,
    fecha date,
    PRIMARY KEY (soldado, servicio),
    CONSTRAINT FKrealizaSoldado FOREIGN KEY(soldado)
      REFERENCES soldado(codSoldado) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT FKrealizaServicio FOREIGN KEY(servicio)
      REFERENCES servicio(codServicio) ON DELETE CASCADE ON UPDATE CASCADE   
    );


-- crear tabla CUARTEL
  DROP TABLE IF EXISTS cuartel;
  CREATE TABLE IF NOT EXISTS cuartel(
    codCuartel int,
    nombre varchar(15),
    direccion varchar(25),
    PRIMARY KEY (codCuartel)
    );

-- crear tabla esta
  CREATE TABLE IF NOT EXISTS esta(
    soldado int,
    cuartel int,
    PRIMARY KEY (soldado, cuartel),
    CONSTRAINT FKestaSoldado FOREIGN KEY(soldado)
      REFERENCES soldado(codSoldado) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT FKestaCuartel FOREIGN KEY(cuartel)
      REFERENCES cuartel(codCuartel) ON DELETE CASCADE ON UPDATE CASCADE   
    );


  -- crear tabla CUERPO
  DROP TABLE IF EXISTS cuerpo;
  CREATE TABLE IF NOT EXISTS cuerpo(
    codCuerpo int,
    denomincacion varchar(15),
    PRIMARY KEY (codCuerpo)
    );

-- crear tabla perteneceCuerpo
  CREATE TABLE IF NOT EXISTS perteneceCuerpo(
    soldado int,
    cuerpo int,
    PRIMARY KEY (soldado, cuerpo),
    CONSTRAINT FKperteneceCuerpoCuerpo FOREIGN KEY(cuerpo)
      REFERENCES cuerpo(codCuerpo) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT FKperteneceCuerpoSoldado FOREIGN KEY(soldado)
      REFERENCES soldado(codSoldado) ON DELETE CASCADE ON UPDATE CASCADE   
    );
